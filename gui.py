from tkinter import *
from model.node import Node
from model.linkedlist import LinkedList

list = LinkedList()

def show_entry_fields():
   print("First Name: %s\nLast Name: %s" % (name.get(), matrics.get()))

def list_students():
    Label(main_page, text="Hello").grid(row=25)
    #print("Hello world")
    list.print_list()

def add_student():
    node = Node(name.get(),matrics.get(),year.get())

    list.add_to_list(node)
    list.print_list()
def find_student():
    marker = list.find(name_for_find.get())
    print(marker.name)
    #Label(find_student_frame, text="Name\tMatrics\tYear", font='bold').grid(row=20, column=0)
    #Label(find_student_frame, text=marker.name+"\t\t"+marker.metric+"\t\t"+marker.year).grid(row=22,column=0)
    Label(find_student_frame, text="Name", font='bold').grid(row=40, column=0)
    Label(find_student_frame, text="Matrics", font='bold').grid(row=40, column=1)
    Label(find_student_frame, text="Year", font='bold').grid(row=40, column=2)

    Label(find_student_frame, text=marker.name).grid(row=43, column=0)
    Label(find_student_frame, text=marker.metric).grid(row=43, column=1)
    Label(find_student_frame, text=marker.year).grid(row=43, column=2)

master = Tk()



def raise_frame(frame):
    frame.tkraise()

def make_window_center():
    w = 500
    h = 350

    ws = master.winfo_screenwidth()
    hs = master.winfo_screenheight()
    x = (ws / 2) - (w / 2)
    y = (hs / 2) - (h / 2)

    master.title('Student database')
    master.geometry('%dx%d+%d+%d' % (w, h, x, y))


make_window_center()
main_page = Frame(master)
main_page.grid(row=0,column=0, sticky='news')



Label(main_page, text="Student database").grid(row=0)

add_student_page_btn = Button(main_page, text="Add student", command=lambda: raise_frame(frame1))
add_student_page_btn.grid(row=5, column=0, sticky='news', padx=10, pady=10)

list_all_students_btn = Button(main_page, text="Get students", padx=10, pady=10, command= lambda : raise_frame(list_students_frame))
list_all_students_btn.grid(row=5, column=1,sticky='news', padx=10, pady=10)

get_students_data_btn = Button(main_page, text="Find student", padx=10, pady=10, command= lambda : raise_frame(find_student_frame))
get_students_data_btn.grid(row=6, column=0,sticky='news', padx=10, pady=10)

frame1 = Frame(master)
frame1.grid(row=0,column=0, sticky='news')

Label(frame1, text="Student Name").grid(row=0)
Label(frame1, text="Matrics").grid(row=1)
Label(frame1, text="Year").grid(row=2)



name = Entry(frame1)
matrics = Entry(frame1)
year = Entry(frame1)
#submit = Button(frame1, text="Back", command= lambda : raise_frame(main_page))

name.grid(row=0, column=1)
matrics.grid(row=1, column=1)
year.grid(row=2, column=1)
#submit.grid(row=5, column=1)

Button(frame1, text='Quit', command=master.quit).grid(row=3, column=0, sticky=W, pady=4)
Button(frame1, text='Add', command=add_student).grid(row=3, column=1, sticky=W, pady=4)
Button(frame1, text='Back', command= lambda  : raise_frame(main_page)).grid(row=3, column=2, sticky=W, pady=4)

find_student_frame = Frame(master)
find_student_frame.grid(row=0, column=0, sticky='news')

Label(find_student_frame, text="Enter student name to search").grid(row=0)

Label(find_student_frame, text="Name").grid(row=2)
name_for_find = Entry(find_student_frame)
name_for_find.grid(row=2, column=1)

Button(find_student_frame, text='Search', command=find_student).grid(row=2, column=3, sticky=W, padx=10,pady=4)
Button(find_student_frame, text='Back', command= lambda  : raise_frame(main_page)).grid(row=2, column=4, sticky=W, padx=10,pady=4)

#canvas = Canvas(find_student_frame)
#canvas.create_line(35, 35, 200, 35)
#canvas.grid(row=10, column=0, sticky='news')


## listing all students

list_students_frame = Frame(master)
list_students_frame.grid(row=0, column=0, sticky='news')

Button(list_students_frame, text='Back', command= lambda  : raise_frame(main_page)).grid(row=2, column=4, sticky=W, padx=10,pady=4)

Label(list_students_frame, text="Name", font='bold').grid(row=40, column=2, padx=20)
Label(list_students_frame, text="Matrics", font='bold').grid(row=40, column=4, padx=20)
Label(list_students_frame, text="Year", font='bold').grid(row=40, column=6, padx=20)

main_page.tkraise()
mainloop( )