from threading import Thread, Condition
from time import sleep

class Producer(object):

    def __init__(self):
        self.products = []
        self.condition = Condition()

    def produce(self):
        self.condition.acquire()

        for i in range(1,5):
            self.products.append("product"+ str(i))
            sleep(0.2)
            print("Item added")

        self.condition.notify()
        self.condition.release()

class Consumer(object):

    def __init__(self, prod):
        self.prod = prod

    def consume(self):
        self.prod.condition.acquire()
        self.prod.condition.wait(timeout=0)
        print("Orders shipped ", self.prod.products)
        self.prod.condition.release()



p = Producer()
c = Consumer(p)

t1 = Thread(target=p.produce())
t2 = Thread(target=c.consume())
t1.start()
t2.start()
