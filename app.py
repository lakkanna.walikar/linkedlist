from model.node import Node
from model.linkedlist import LinkedList

class App(object):

    def print_format(self, marker):
        print("{}: {} (year {})".format(marker.name, marker.metric, marker.year))

    def __init__(self):
        self.list = LinkedList()
        print("Linked list ")
        self.userWants = True
        while self.userWants:
            print("1. add student\t 2. get root\t 3. print list\t 4. find\t 5. delete\n")
            try:
                self.userInput = int(input("Enter your choice: "))
            except ValueError:
                print("selection should be a digit")
            if self.userInput == 1:

                self.name = input("enter a name: ")
                try:
                    self.metric = int(input("enter a metric: "))
                except ValueError:
                    self.metric = None
                    print("please provide number!")
                try:
                    self.year = int(input("enter a year: "))
                except ValueError:
                    self.year = None
                    print("please provide year in digits!!")

                self.node = Node(self.name, self.metric, self.year)
                self.list.add_to_list(self.node)
                print()
                marker = self.list.get_root()
                self.print_format(marker)
                print("\nadded succussfully!")

            elif self.userInput == 2:
                try:
                    print("Root: ")
                    marker = self.list.get_root()
                    self.print_format(marker)
                except AttributeError:
                    print("\nList is empty\n")

            elif self.userInput == 3:
                self.list.print_list()


            elif self.userInput == 4:
                self.searchname = input("enter name to find: ")
                try:
                    marker = self.list.find(self.searchname)
                    self.print_format(marker)
                except:
                    print("Name {} was not found in the linked list.".format(self.searchname))
            elif self.userInput == 5:
                #last_student = self.list.print_list()[:-1]
                self.list.print_list().pop()
            ask_user = input("\nWould like to continue:(y/n) ")
            if ask_user.lower() == 'n':
                self.userWants = False


if  __name__ == '__main__':
    app = App()
