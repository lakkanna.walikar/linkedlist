class Node(object):
    def __init__(self, name, metric, year):
        self.name = name
        self.metric = metric
        self.year = year
        self.__next = None

    def set_next(self, node):
        if isinstance(node, Node) or node is None:
            self.__next = node
        else:
            raise TypeError("The 'next' node is none")

    def get_next(self):
        return self.__next

    def print_details(self):
        print("{}: {} (year {})". format(self.name,self.metric,self.year))
